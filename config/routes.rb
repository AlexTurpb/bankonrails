Rails.application.routes.draw do

  devise_for :users

  root to: 'profile#dashboard'

  resources :balances do
    post :change_balance, on: :collection
  end

  resources :accounts
  resources :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
