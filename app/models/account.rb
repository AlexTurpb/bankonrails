class Account < ApplicationRecord
  belongs_to :user
  enum currency: [:uah, :usd]
end
