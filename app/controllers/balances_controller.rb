class BalancesController < ApplicationController
  def change_balance
    @form_object = FormObject.new(params).call
    @form_object.save
    redirect_to root_path
  end
end
