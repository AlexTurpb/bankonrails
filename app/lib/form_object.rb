class FormObject
  attr_reader :params
  def initialize(params)
    @params = params
  end

  def call
    acc = Account.find(params[:currency])
    acc.amount = acc.amount + params[:amount].to_f * (params[:operation] == 'subtr' ? -1 : 1)
    acc
  end
end
