# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

seed = YAML.load_file('config/data_seed.yml')

seed['users'].keys.each_with_index do |key, index|
  user_data = seed['users'][key]
  user = User.create!(
    name: user_data['name'],
    email: "pivo_#{index}@rak.com",
    password: user_data['password'],
    login: key
  )
  seed['accounts'].keys.each_slice(2).to_a[index].each do |acc_data|
    Account.create!(
      user: user,
      currency: seed['accounts'][acc_data]['currency'],
      amount: seed['accounts'][acc_data]['balance']
    )
  end
end

# require 'yaml'

# data = YAML.load_file('config/data_seed2.yml')

# data['users'].each do |id, user_data|
#   User.create!(name: user_data['name'], password: user_data['password'], email: user_data['email'])
# end

# data['accounts'].each do |account_id, account_data|
#   Account.create!(currency: account_data['currency'], amount: account_data['balance'], user_id: account_data['user_id'])
# end
